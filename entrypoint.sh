#!/usr/bin/env bash

set -eo pipefail

if [ "$1" != "confluence" ]
then
    exec su-exec ${APP_USER} "$@"
fi

# remove dummy confluence argument
ARGS="${@:2}"

# reset tomcat connector configuration
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8090']/@proxyName" "${CONFLUENCE_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8090']/@proxyPort" "${CONFLUENCE_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8090']/@scheme" "${CONFLUENCE_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --delete "//Connector[@port='8090']/@secure" "${CONFLUENCE_INSTALL}/conf/server.xml"
xmlstarlet edit --inplace --pf --ps --update "//Connector[@port='8090']/@redirectPort" --value "8443" "${CONFLUENCE_INSTALL}/conf/server.xml"

if [ -n "${X_PROXY_NAME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8090']" --type "attr" --name "proxyName" --value "${X_PROXY_NAME}" "${CONFLUENCE_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_PORT}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8090']" --type "attr" --name "proxyPort" --value "${X_PROXY_PORT}" "${CONFLUENCE_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PROXY_SCHEME}" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8090']" --type "attr" --name "scheme" --value "${X_PROXY_SCHEME}" "${CONFLUENCE_INSTALL}/conf/server.xml"
fi

if [ "${X_PROXY_SCHEME}" = "https" ]
then
    xmlstarlet edit --inplace --pf --ps --insert "//Connector[@port='8090']" --type "attr" --name "secure" --value "true" "${CONFLUENCE_INSTALL}/conf/server.xml"
    xmlstarlet edit --inplace --pf --ps --update "//Connector[@port='8090']/@redirectPort" --value "${X_PROXY_PORT}" "${CONFLUENCE_INSTALL}/conf/server.xml"
fi

if [ -n "${X_PATH}" ]
then
    xmlstarlet edit --inplace --pf --ps --update '//Context/@path' --value "${X_PATH}" "${CONFLUENCE_INSTALL}/conf/server.xml"
fi

umask 0027
chown -R ${APP_USER}:${APP_GROUP} ${CONFLUENCE_HOME}
exec su-exec ${APP_USER} ${CONFLUENCE_INSTALL}/bin/start-confluence.sh -fg ${ARGS}
