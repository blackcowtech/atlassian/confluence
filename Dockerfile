FROM blackcowtech/atlassian-base
LABEL maintainer="Paul Jefferson <paul.jefferson@blackcow-technology.co.uk>"

ARG CONFLUENCE_VERSION

ENV \
  CONFLUENCE_HOME=${APP_HOME}/confluence \
  CONFLUENCE_INSTALL=${APP_INSTALL}/confluence

EXPOSE 8090
EXPOSE 8091

# from https://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz
COPY atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz /tmp/

RUN set -ex \
  && apk add --update --no-cache --virtual .build-deps \
  shadow \
  tar \
  && mkdir -p ${CONFLUENCE_HOME} \
  && mkdir -p ${CONFLUENCE_INSTALL} \
  && usermod -d ${CONFLUENCE_HOME} ${APP_USER} \
  && tar xzf /tmp/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz -C ${CONFLUENCE_INSTALL} --strip-components=1 \
  && echo "confluence.home=${CONFLUENCE_HOME}" > ${CONFLUENCE_INSTALL}/confluence/WEB-INF/classes/confluence-init.properties \
  && chown -R ${APP_USER}:${APP_GROUP} ${CONFLUENCE_HOME} \
  && chown -R ${APP_USER}:${APP_GROUP} ${CONFLUENCE_INSTALL} \
  # tidy up
  && apk del .build-deps \
  && rm -rf /tmp/* \
  && rm -rf /var/log/*

WORKDIR ${CONFLUENCE_HOME}
VOLUME ["${CONFLUENCE_HOME}"]

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/sbin/tini","--", "/entrypoint.sh"]
CMD ["confluence"]
